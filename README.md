# Contributions

By default accounts do not have rights to create personal repositories and therefore fork existing projects. This guide outlines how to enable personal repositories in order to contribute to projects hosted on gitlab.arm.com.

## Terms of Service

Thank you for your interest in participating in Arm Limited's ("Arm") GitLab instance.  The following Terms of Service govern your participation in this GitLab instance, and by following this process you agree to abide by these Terms of Service.  These Terms of Service supplement any other applicable terms, conditions, policies, or obligations that may apply with respect to your relationship with GitLab, Arm, or otherwise in connection with your participation.

 - Arm agrees to allow you to create personal repositories within Arm's GitLab instance for purposes of forking, and submitting patches to, projects contained within Arm's GitLab instance.  You are responsible for the content in your personal repositories, and you agree to comply with any licenses applicable to such projects.  You further agree to maintain such personal repositories as private.
 - Arm reserves the right, in its sole discretion, to decide whether to accept and/or merge any patches or other contributions you submit.  Arm further reserves the right to take any action with respect to the content, organization, or review of this GitLab instance, including your personal repositories.
 - Arm reserves the right to revoke the permissions granted herein or otherwise disable your access at its discretion.
 - You agree to refrain from taking any action that would cause Arm to be in violation of the GitLab Open Source Program Agreement (https://about.gitlab.com/handbook/legal/opensource-agreement/).
 - You agree to not perform any continuous integration or other testing of forks in your personal repositories, and to further not attempt to gain access to Arm resources for cryptocurrency mining.
 - You agree that your participation in this GitLab instance is voluntary and shall in no way render you an employee, officer, agent, joint venturer, or contractor of Arm.

## Requesting Access

In order to contribute to a project, it is necessary to fork the project repository. The ability to fork a project repository is disabled by default, so we ask that an issue is raised [here](https://gitlab.arm.com/documentation/contributions/-/issues). This is a one time activity. By raising the issue you are agreeing to all the terms of service mentioned above.

An admin will then process this request and advise once complete. At this point you will then be able to fork the relevant projects and contribute. For project specfic contribution models, please refer to the project's own contributions guide.

The project limit for individual user is set to 10. When you hit the project limit, raise the issue [here](https://gitlab.arm.com/documentation/contributions/-/issues) to increase the limit.
